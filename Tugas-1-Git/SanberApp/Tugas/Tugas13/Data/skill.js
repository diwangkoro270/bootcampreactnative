const Data = [
  {
    id: '1',
    image: require('../asset/js.png'),
    name: 'JavaScript',
    level_text: 'Basic',
    category: 'Language',
    level: 70,
  },
  {
    id: '2',
    image: require('../asset/laravel.png'),
    name: 'Laravel',
    level_text: 'Medium',
    category: 'Framework',
    level: 40,
  },
];

export {Data};
