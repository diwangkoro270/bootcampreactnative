/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import 'react-native-gesture-handler';

import firebase from '@react-native-firebase/app';

import Index from './Tugas/Tugas15/index';
import RestApis from './Tugas/Tugas14/restapi';
import Quiz from './Quiz3/index';
import CodedIn from './Tugas/Tugas13/Router/index';

var firebaseConfig = {
  apiKey: 'AIzaSyAu9o_JJwBM00y2cls4dj3FPBO-vDZBTaA',
  authDomain: 'codedin-4ef9a.firebaseapp.com',
  projectId: 'codedin-4ef9a',
  storageBucket: 'codedin-4ef9a.appspot.com',
  messagingSenderId: '885196364174',
  appId: '1:885196364174:web:1a1d7237bfffcd04572cdf',
  measurementId: 'G-94P5CT61DG',
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{flex: 1}}>
        {/* <Index></Index> */}
        {/* <RestApis></RestApis> */}
        {/* <Quiz></Quiz> */}
        <CodedIn></CodedIn>
      </SafeAreaView>
    </>
  );
};

export default App;
